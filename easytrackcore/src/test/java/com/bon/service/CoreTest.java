package com.bon.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.bon.util.CoreUtil;
import com.bon.vo.Road;
import com.bon.vo.RoadMap;

@RunWith(JUnit4.class)
public class CoreTest extends TestCase {
	private RoadMap roadmap1;
	private RoadMap roadmap2;
	private RoadMap roadmap3;
	private String jsonRoadMap = "{\r\n\t\t    \"cityName\": \"S\u00E3o Paulo\",\r\n\t\t    \"roadMap\": [\r\n\t\t        {\r\n\t\t            \"source\": \"A\",\r\n\t\t            \"destiny\": \"B\",\r\n\t\t            \"distance\": \"10\"\r\n\t\t        },\r\n\t\t        {\r\n\t\t            \"source\": \"B\",\r\n\t\t            \"destiny\": \"C\",\r\n\t\t            \"distance\": \"20\"\r\n\t\t        }\r\n\t\t    ]\r\n\t\t}";
	// Invalid cityName key
	private String invalid_jsonRoadMap = "{\r\n\t\t    \"cityNames\": \"S\u00E3o Paulo\",\r\n\t\t    \"roadMap\": [\r\n\t\t        {\r\n\t\t            \"source\": \"A\",\r\n\t\t            \"destiny\": \"B\",\r\n\t\t            \"distance\": \"10\"\r\n\t\t        },\r\n\t\t        {\r\n\t\t            \"source\": \"B\",\r\n\t\t            \"destiny\": \"C\",\r\n\t\t            \"distance\": \"20\"\r\n\t\t        }\r\n\t\t    ]\r\n\t\t}";
	
	@Before
	public void setUp() {
		loadRoadMaps();
		createTmpDir();
		System.setProperty("roadmap.file.dir", new File("tmp").getAbsolutePath());
	}

	private void loadRoadMaps() {
		roadmap1 = new RoadMap("A", "B", "C", "D", "E");
		roadmap1.connect("A", "B", 5);
		roadmap1.connect("B", "C", 3);
		roadmap1.connect("D", "E", 6);
		roadmap1.connect("B", "D", 2);
		roadmap1.connect("E", "D", 4);
		roadmap1.connect("B", "E", 9);
		roadmap1.connect("D", "A", 7);
		roadmap1.connect("C", "B", 2);
		roadmap1.connect("A", "C", 10);
		roadmap1.connect("C", "E", 1);

		roadmap2 = new RoadMap("A", "B", "C", "D", "E");
		roadmap2.connect("A", "B", 10);
		roadmap2.connect("B", "D", 15);
		roadmap2.connect("A", "C", 20);
		roadmap2.connect("C", "D", 30);
		roadmap2.connect("B", "E", 50);
		roadmap2.connect("D", "E", 30);
		
		roadmap3 = new RoadMap("A", "B", "C");
		roadmap3.connect("A", "B", 10);
		roadmap3.connect("B", "C", 20);
	}
	
	@After
	public void clean() {
		deleteTmpDir();
	}
	
	private void deleteTmpDir() {
		File tmpDir = new File("tmp");
		if (tmpDir.exists()) {
			try {
				FileUtils.forceDelete(tmpDir);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	private void createTmpDir() {
		File tmpDir = new File("tmp");
		if (!tmpDir.exists()) {
			try {
				FileUtils.forceMkdir(tmpDir);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	@Test
	public void testFindShortestPath() {
		List<Road> path = Core.findShortestPath(roadmap1, "A", "E");
		assertEquals(3, path.size());
		assertEquals("(A : B)", path.get(0).toString());
		assertEquals("(B : C)", path.get(1).toString());
		assertEquals("(C : E)", path.get(2).toString());
	}

	@Test(expected = RuntimeException.class)
	public void testFindShortestPath_NullMap() {
		RoadMap roadMap = null;
		Core.findShortestPath(roadMap, "A", "E");
	}

	@Test(expected = RuntimeException.class)
	public void testFindShortestPath_NullSourceOrDestiny() {
		RoadMap roadMap = new RoadMap("A", "B");
		try {
			Core.findShortestPath(roadMap, null, "A");
		} catch (Exception e) {
			Core.findShortestPath(roadMap, "A", null);
		}
	}

	@Test
	public void testFindShortestPath_NoPath() {
		RoadMap roadMap = new RoadMap("A", "B");
		List<Road> path = Core.findShortestPath(roadMap, "A", "B");
		assertNull(path);

		roadMap = new RoadMap("A", "B", "C", "D", "E");
		roadMap.connect("A", "B", 5);
		roadMap.connect("D", "E", 6);
		path = Core.findShortestPath(roadMap, "A", "E");
		assertNull(path);
	}

	@Test
	public void testFindCost() {
		List<Road> path = Core.findShortestPath(roadmap2, "A", "D");
		double cost = Core.findCost(path, 10.0, 2.5);

		assertEquals(6.25, cost);
	}

	@Test(expected = RuntimeException.class)
	public void testFindCostError() {
		try {
			Core.findCost(null, 10.0, 2.5);
		} catch (Exception e) {
			List<Road> path = Core.findShortestPath(roadmap2, "A", "D");
			try {
				Core.findCost(path, 0.0, 2.5);
			} catch (Exception ex) {
				Core.findCost(path, 2.5, 0.0);
			}
		}
	}

	@Test
	public void testSaveMap() {
		Core.saveMap(jsonRoadMap);
		String jsonRoadMapSaved = CoreUtil.readFile("São Paulo");
		assertEquals(jsonRoadMap, jsonRoadMapSaved);
	}
	
	@Test(expected = RuntimeException.class)
	public void testSaveMap_IvalidParameter() {
		Core.saveMap(invalid_jsonRoadMap);
	}
	
	@Test
	public void testLoad() {
		Core.saveMap(jsonRoadMap);
		RoadMap roadMap = Core.load("São Paulo");
		assertEquals(roadmap3.toString(), roadMap.toString());
	}
	
	@Test(expected = RuntimeException.class)
	public void testLoad_Error() {
		RoadMap roadMap = Core.load("XWZ");
	}
}
