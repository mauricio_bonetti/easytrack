package com.bon.service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jgrapht.alg.DijkstraShortestPath;

import com.bon.util.CoreUtil;
import com.bon.util.JSONUtil;
import com.bon.vo.Road;
import com.bon.vo.RoadMap;

/**
 * This class is responsible for business evaluations for the shortest path
 * problem
 * 
 * @author Mauricio Bonetti
 * 
 */
public class Core {
	/**
	 * This method finds the shortest path between source and destiny against a
	 * saved map
	 * 
	 * @param roadmap
	 * @param source
	 * @param destiny
	 * @return An order list of roads in order to reach the destiny
	 */
	public static List<Road> findShortestPath(RoadMap roadmap, String source,
			String destiny) {
		if (roadmap == null || StringUtils.isBlank(source)
				|| StringUtils.isBlank(destiny)) {
			throw new RuntimeException(
					"Invalid parameters for Core.findShortestPath");
		}
		return DijkstraShortestPath.findPathBetween(roadmap, source, destiny);
	}

	/**
	 * This method evaluates the total cost of a trip following the road
	 * 
	 * @param path
	 * @param kmPerLitre
	 * @param costPerLitre
	 * @return Trip's total cost
	 */
	public static double findCost(List<Road> path, double kmPerLitre,
			double costPerLitre) {
		if (path == null || kmPerLitre <= 0.0 || costPerLitre <= 0) {
			throw new RuntimeException("Invalid parameters for Core.findCost");
		}
		double distance = 0.0;
		for (Road road : path) {
			distance += road.getDistance();
		}
		double litres = distance / kmPerLitre;
		return litres * costPerLitre;
	}

	/**
	 * This method saves a json roadmap into file system. It's important to
	 * notice that in order accomplish it's task is necessary to have the system
	 * property "roadmap.file.dir" correctly set
	 * 
	 * @param jsonRoadMap
	 */
	public static void saveMap(String jsonRoadMap) {
		if (StringUtils.isBlank(jsonRoadMap)) {
			throw new RuntimeException("Invalid parameters for Core.saveMap");
		}
		String filename = JSONUtil.getCityName(jsonRoadMap);
		CoreUtil.writeFile(filename, jsonRoadMap);
	}

	/**
	 * This method is responsible for reading the file containing the city's
	 * road map and builds the RoadMap object
	 * 
	 * @param cityName
	 * @return
	 */
	public static RoadMap load(String cityName) {
		String jsonRoadMap = CoreUtil.readFile(cityName);
		Map<String, Object> map = JSONUtil.getRoadMap(jsonRoadMap);
		List<Map<String, Object>> roads = (List<Map<String, Object>>) map
				.get("roadMap");
		Set<String> roadsName = new HashSet<String>();
		for (Map<String, Object> road : roads) {
			roadsName.add((String) road.get("source"));
			roadsName.add((String) road.get("destiny"));
		}
		RoadMap roadMap = new RoadMap(roadsName);
		for (Map<String, Object> road : roads) {
			roadMap.connect((String) road.get("source"),
					(String) road.get("destiny"),
					Double.parseDouble("" + road.get("distance")));
		}
		return roadMap;
	}
}
