package com.bon.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * This class provides some utils in order to accomplish the shortest path
 * problem task.
 * 
 * @author Mauricio Bonetti
 * 
 */
public class CoreUtil {
	private static final String fileDir = System
			.getProperty("roadmap.file.dir");

	/**
	 * The system property "roadmap.file.dir" must be set!
	 */
	private static void assertfileDir() {
		if (StringUtils.isBlank(fileDir)) {
			throw new RuntimeException(
					"Property roadmap.file.dir not set, check input parameters");
		}
	}

	/**
	 * This method is responsible for reading the file containing the city's
	 * road map and returns its content
	 * 
	 * @param cityName
	 * @return the content of cityName file
	 */
	public static String readFile(String cityName) {
		assertfileDir();
		String filename = fileDir + File.separator + cityName;
		File file = new File(filename);
		try {
			return FileUtils.readFileToString(file, "UTF-8");
		} catch (IOException e) {
			throw new RuntimeException("Unable to read file: " + filename, e);
		}
	}

	/**
	 * Writes the cityName roadmap file
	 * @param cityName
	 * @param roadMap
	 */
	public static void writeFile(String cityName, String roadMap) {
		assertfileDir();
		String filename = fileDir + File.separator + cityName;
		File file = new File(filename);
		try {
			FileUtils.write(file, roadMap, "UTF-8");
		} catch (IOException e) {
			throw new RuntimeException("Unable to write to file: " + filename,
					e);
		}
	}
}
