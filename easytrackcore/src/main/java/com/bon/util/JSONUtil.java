package com.bon.util;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * This class provides some utils in order to handle JSON
 * 
 * @author Mauricio Bonetti
 * 
 */
public class JSONUtil {

	/**
	 * This method takes the cityName from a jsonRoadMap String
	 * 
	 * @param jsonRoadMap
	 * @return
	 */
	public static String getCityName(String jsonRoadMap) {
		Map<String, Object> roadMap = getRoadMap(jsonRoadMap);
		String cityName = (String) roadMap.get("cityName");
		if (StringUtils.isBlank(cityName)) {
			throw new RuntimeException(
					"Unable to read city name from parameter on JSONUtil.getRoadMap");
		}
		return cityName;
	}

	/**
	 * This method takes a roadMap JSON string and returns a map with each of
	 * its properties set
	 * 
	 * @param jsonRoadMap
	 * @return
	 */
	public static Map<String, Object> getRoadMap(String jsonRoadMap) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(jsonRoadMap, Map.class);
		} catch (Exception e) {
			throw new RuntimeException(
					"Unable to read map from parameter on JSONUtil.getRoadMap",
					e);
		}
	}

	/**
	 * This method takes an object and converts it into a JSON String
	 * 
	 * @param obj
	 * @param c
	 * @return
	 */
	public static <T extends Object> String toJson(T obj, Class c) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			Object json = objectMapper.readValue(
					objectMapper.writeValueAsString(obj), c);
			return objectMapper.writeValueAsString(json);
		} catch (Exception e) {
			throw new RuntimeException(
					"Unable to convert object to Json string");
		}
	}

	/**
	 * This method takes an object and converts it into a JSON String
	 * 
	 * @param v
	 * @return
	 */
	public static <T extends Object> String toJson(T v) {
		return toJson(v, v.getClass());
	}
}
