package com.bon.vo;

import org.jgrapht.graph.DefaultWeightedEdge;

/**
 * This object is a businness representation of a graph's edge. It contains a
 * distance in order to represet graph's wheight
 * 
 * @author Mauricio Bonetti
 * 
 */
public class Road extends DefaultWeightedEdge {
	private double distance;

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}
}
