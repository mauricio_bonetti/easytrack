package com.bon.vo;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.jgrapht.graph.SimpleDirectedWeightedGraph;

/**
 * This object is a businness representation of a directed graph.
 * 
 * @author Mauricio Bonetti
 * 
 */
public class RoadMap extends SimpleDirectedWeightedGraph<String, Road> {

	/**
	 * This construction takes a set of RoadsName and add it as a vertex to the
	 * graph
	 * 
	 * @param roadsName
	 */
	public RoadMap(Set<String> roadsName) {
		super(Road.class);
		for (String road : roadsName) {
			this.addVertex(road);
		}
	}

	/**
	 * This construction takes an array of RoadsName and add it as a vertex to
	 * the graph
	 * 
	 * @param roadsName
	 */
	public RoadMap(String... roadsName) {
		this(new HashSet<String>(Arrays.asList(roadsName)));
	}

	/**
	 * This method adds a connection (road) between two points
	 * 
	 * @param source
	 * @param destiny
	 * @param distance
	 */
	public void connect(String source, String destiny, double distance) {
		Road road = addEdge(source, destiny);
		road.setDistance(distance);
		setEdgeWeight(road, distance);
	}

	public String toString() {
		StringBuilder roadMap = new StringBuilder();
		Set<Road> roads = edgeSet();
		for (Road road : roads) {
			String source = getEdgeSource(road);
			String target = getEdgeTarget(road);
			double distance = getEdgeWeight(road);
			roadMap.append(source);
			roadMap.append("\t");
			roadMap.append(target);
			roadMap.append("\t");
			roadMap.append(distance);
			roadMap.append("\n");
		}
		return roadMap.toString();
	}
}
