Description
-----------

Easy Track is a solution to the problem of the shortest path. It consists of three modules:


**EasyTrackCore**

Responsible for persisting maps, calculate the shortest path from a route and answer
the total cost of the trip. The route is calculated according to the Dijkstra algorithm
with performance for the worst case O(|E| + |V| * log|V|) 
(http://en.wikipedia.org/wiki/Dijkstra's_algorithm)


**EasyTrackWs**

Webservice with two interfaces, "saveMap" and "findShortestPath". This module is
responsible for receiving requests, validate, and invoke the core methods.

  
**EasyTrackServer**

Module with an embedded web server (Jetty). It accounts for running up a server on
port 8080 and to deploy the webservice (Jersey) at 

```
#!java

localhost:8080\easytrack\ws
```


QuickStart
------------------

Checkout master of modules:
```
#!java

https://mauricio_bonetti@bitbucket.org/mauricio_bonetti/easytrack.git
```
Install modules in sequence:
```
#!java
mvn install (easytrackcore)
mvn install (easytrackws)
mvn install (easytrackserver)
```
By doing this, it will be installed into target directory of easytrackserver the file easytrackserver-1.0.jar and a lib directory with all its dependencies (easytrackws, easytrackcore, jetty, jersey...).
At target directory of easytrackserver execute:
```
#!java
(Windows)
java -Droadmap.file.dir=S:\dev\roadmap -cp ".\*;lib\*" com.bon.server.ServerStarter
```
With this, we have the server running on port 8080 with the service installed. On a
HTTP client, perform load a map, called the interface "saveMap" as:
```
#!java

Url: localhost:8080\easytrack\ws
POST parameter: 

```
```
#!java


{
    "type": "jsonwsp/request",
    "version": "1.0",
    "methodname": "saveMap",
    "args": {
        "cityName": "Wall",
        "roadMap": [
            {
                "source": "A",
                "destiny": "B",
                "distance": 10
            },
            {
                "source": "B",
                "destiny": "D",
                "distance": 15
            },
            {
                "source": "A",
                "destiny": "C",
                "distance": 20
            },
            {
                "source": "C",
                "destiny": "D",
                "distance": 30
            },
            {
                "source": "B",
                "destiny": "E",
                "distance": 50
            },
            {
                "source": "D",
                "destiny": "E",
                "distance": 30
            }
        ]
    },
    "mirror": {
        "id": 1
    }
}

```


Service response:


```
#!java

{
    "result": {
        "success": true
    },
    "servicename": "easytrackws",
    "reflection": {
        "id": "1"
    },
    "method": "saveMap",
    "type": "jsonwsp/response",
    "version": "1.0"
}
```

Thus, we have the map to the city "Wall" saved in the "S:\dev\roadmap".
So we can get the shortest path to a delivery in that city, calling the
method "findShortestPath":

```
#!java

Url: localhost:8080\easytrack\ws
Parâmetro do POST:
```
```
#!java


{
    "type": "jsonwsp/request",
    "version": "1.0",
    "methodname": "findShortestPath",
    "args": {
        "cityName": "Wall",
        "source": "A",
        "destiny": "D",
        "autonomy": 10,
        "fuelCost": 2.5
    },
    "mirror": {
        "id": 1
    }
}
```

The service responds to the routing information and the total cost of the trip:

```
#!java

{
    "result": {
        "totalCost": 6.25,
        "path": [
            "A",
            "B",
            "D"
        ],
        "success": true
    },
    "servicename": "easytrackws",
    "reflection": {
        "id": "1"
    },
    "method": "findShortestPath",
    "type": "jsonwsp/response",
    "version": "1.0"
}
```