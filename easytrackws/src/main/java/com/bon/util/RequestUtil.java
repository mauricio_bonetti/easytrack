package com.bon.util;

import org.apache.commons.lang3.StringUtils;

import com.bon.util.RequestValidator.SERVICE_INTERFACE;
import com.bon.vo.Request;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class contains some utils in order to handle the webservice requests
 * 
 * @author Mauricio Bonetti
 * 
 */
public class RequestUtil {

	/**
	 * This method builds a Request POJO from a JSON-WSP request
	 * 
	 * @param jsonRequest
	 * @return Request POJO
	 */
	public static Request loadRequest(String jsonRequest) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			Request request = mapper.readValue(jsonRequest, Request.class);
			return request;
		} catch (Exception e) {
			throw new RuntimeException("Unable to parse JSON to Java Object", e);
		}
	}

	/**
	 * This method decides which service interface the request is attempting to
	 * invoke
	 * 
	 * @param methodName
	 * @return Requests correct service interface
	 */
	public static SERVICE_INTERFACE getServiceInterface(String methodName) {
		SERVICE_INTERFACE serviceMethod;
		if (StringUtils.equalsIgnoreCase(methodName, "findShortestPath")) {
			serviceMethod = SERVICE_INTERFACE.FIND_SHORTEST_PATH;
		} else if (StringUtils.equalsIgnoreCase(methodName, "saveMap")) {
			serviceMethod = SERVICE_INTERFACE.SAVE_MAP;
		} else {
			throw new RuntimeException("There is no service called "
					+ methodName);
		}
		return serviceMethod;
	}
}
