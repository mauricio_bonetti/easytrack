package com.bon.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.report.ProcessingReport;
import com.github.fge.jsonschema.util.JsonLoader;

/**
 * This class provides utils for request assertions
 * 
 * @author Mauricio Bonetti
 * 
 */
public class RequestValidator {
	public enum SERVICE_INTERFACE {
		SAVE_MAP, FIND_SHORTEST_PATH
	}

	/**
	 * This method checks the JSON-WSP request against a schema for each valid
	 * webservices interface
	 * 
	 * @param jsonRequest
	 * @param i
	 */
	public static void assertRequest(String jsonRequest, SERVICE_INTERFACE i) {
		String schema = null;
		switch (i) {
		case SAVE_MAP:
			schema = "{\"type\":\"object\",\"$schema\": \"http://json-schema.org/draft-03/schema\",\"id\": \"http://jsonschema.net\",\"required\":true,\"properties\":{ \"args\": { \"type\":\"object\", \"id\": \"http://jsonschema.net/args\", \"required\":true, \"properties\":{ \"cityName\": { \"type\":\"string\", \"id\": \"http://jsonschema.net/args/cityName\", \"required\":true }, \"roadMap\": { \"type\":\"array\", \"id\": \"http://jsonschema.net/args/roadMap\", \"required\":true, \"items\": { \"type\":\"object\", \"id\": \"http://jsonschema.net/args/roadMap/0\", \"required\":true, \"properties\":{ \"destiny\": { \"type\":\"string\", \"id\": \"http://jsonschema.net/args/roadMap/0/destiny\", \"required\":true }, \"distance\": { \"type\":\"number\", \"minimum\": 0, \"id\": \"http://jsonschema.net/args/roadMap/0/distance\", \"required\":true }, \"source\": { \"type\":\"string\", \"id\": \"http://jsonschema.net/args/roadMap/0/source\", \"required\":true } } } } } }, \"methodname\": { \"type\":\"string\", \"id\": \"http://jsonschema.net/methodname\", \"required\":true }, \"mirror\": { \"type\":\"object\", \"id\": \"http://jsonschema.net/mirror\", \"required\":true, \"properties\":{ \"id\": { \"type\":\"number\", \"id\": \"http://jsonschema.net/mirror/id\", \"required\":true } } }, \"type\": { \"type\":\"string\", \"id\": \"http://jsonschema.net/type\", \"required\":true }, \"version\": { \"type\":\"string\", \"id\": \"http://jsonschema.net/version\", \"required\":true } }}";
			break;
		case FIND_SHORTEST_PATH:
			schema = "{\"type\":\"object\",\"$schema\": \"http://json-schema.org/draft-03/schema\",\"id\": \"http://jsonschema.net\",\"required\":true,\"properties\":{ \"args\": { \"type\":\"object\", \"id\": \"http://jsonschema.net/args\", \"required\":true, \"properties\":{ \"autonomy\": { \"type\":\"number\", \"minimum\": 0, \"id\": \"http://jsonschema.net/args/autonomy\", \"required\":true }, \"cityName\": { \"type\":\"string\", \"id\": \"http://jsonschema.net/args/cityName\", \"required\":true }, \"destiny\": { \"type\":\"string\", \"id\": \"http://jsonschema.net/args/destiny\", \"required\":true }, \"fuelCost\": { \"type\":\"number\", \"minimum\": 0, \"id\": \"http://jsonschema.net/args/fuelCost\", \"required\":true }, \"source\": { \"type\":\"string\", \"id\": \"http://jsonschema.net/args/source\", \"required\":true } } }, \"methodname\": { \"type\":\"string\", \"id\": \"http://jsonschema.net/methodname\", \"required\":true }, \"mirror\": { \"type\":\"object\", \"id\": \"http://jsonschema.net/mirror\", \"required\":true, \"properties\":{ \"id\": { \"type\":\"number\", \"id\": \"http://jsonschema.net/mirror/id\", \"required\":true } } }, \"type\": { \"type\":\"string\", \"id\": \"http://jsonschema.net/type\", \"required\":true }, \"version\": { \"type\":\"string\", \"id\": \"http://jsonschema.net/version\", \"required\":true } }}";
			break;
		}
		ProcessingReport report = null;
		try {
			JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
			JsonNode schemaNode = JsonLoader.fromString(schema);
			JsonNode data = JsonLoader.fromString(jsonRequest);
			JsonSchema jsonSchema = factory.getJsonSchema(schemaNode);
			report = jsonSchema.validate(data);
		} catch (Exception e) {
			throw new RuntimeException("Error evaluating request from schema: "
					+ schema, e);
		}
		if (!report.isSuccess()) {
			throw new RuntimeException("Invalid request from schema: " + schema
					+ "\n" + report.toString());
		}
	}
}
