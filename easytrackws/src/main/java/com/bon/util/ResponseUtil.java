package com.bon.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.exception.ExceptionUtils;

import com.bon.vo.Request;
import com.bon.vo.Response;

/**
 * This class contains some utils in order to handle the webservice responses
 * 
 * @author Mauricio Bonetti
 * 
 */
public class ResponseUtil {

	/**
	 * This method builds a successful saveMap response
	 * 
	 * @param request
	 * @return
	 */
	public static Response loadSuccessSaveMapResponse(Request request) {
		Response resp = new Response();
		resp.setType("jsonwsp/response");
		resp.setVersion(request.getVersion());
		resp.setServicename("easytrackws");
		resp.setMethod(request.getMethodname());
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", true);
		resp.setResult(result);
		Map<String, String> reflection = new HashMap<String, String>();
		reflection.put("id", request.getMirror().get("id"));
		resp.setReflection(reflection);
		return resp;
	}

	/**
	 * This method builds a successful FindShortestPath response
	 * 
	 * @param request
	 * @param shortestPath
	 * @return
	 */
	public static Response loadSuccessFindShortestPathResponse(Request request,
			Map<String, Object> shortestPath) {
		Response resp = new Response();
		resp.setType("jsonwsp/response");
		resp.setVersion(request.getVersion());
		resp.setServicename("easytrackws");
		resp.setMethod(request.getMethodname());
		Map<String, Object> result = new HashMap<String, Object>(shortestPath);
		result.put("success", true);
		resp.setResult(result);
		Map<String, String> reflection = new HashMap<String, String>();
		reflection.put("id", request.getMirror().get("id"));
		resp.setReflection(reflection);
		return resp;
	}

	/**
	 * Loads an error response
	 * 
	 * @param request
	 * @param e
	 * @return
	 */
	public static Response loadErrorResponse(Request request, Exception e) {
		return loadErrorResponse(e, request.getMethodname(), request
				.getMirror().get("id"));
	}

	/**
	 * Loads the default error response
	 * 
	 * @param e
	 * @return
	 */
	public static Response loadDefaultErrorResponse(Exception e) {
		return loadErrorResponse(e, "null", "null");
	}

	/**
	 * Loads an error response
	 * 
	 * @param e
	 * @param errorMethod
	 * @param mirrorId
	 * @return
	 */
	private static Response loadErrorResponse(Exception e, String errorMethod,
			String mirrorId) {
		Response resp = new Response();
		resp.setType("jsonwsp/response");
		resp.setVersion("1.0");
		resp.setServicename("easytrackws");
		resp.setMethod(errorMethod);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", false);
		result.put("stack", ExceptionUtils.getStackTrace(e));
		resp.setResult(result);
		Map<String, String> reflection = new HashMap<String, String>();
		reflection.put("id", mirrorId);
		resp.setReflection(reflection);
		return resp;
	}
}
