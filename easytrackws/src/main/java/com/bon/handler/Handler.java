package com.bon.handler;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bon.service.EasyTrack;
import com.bon.util.JSONUtil;
import com.bon.util.RequestUtil;
import com.bon.util.RequestValidator;
import com.bon.util.RequestValidator.SERVICE_INTERFACE;
import com.bon.util.ResponseUtil;
import com.bon.vo.Request;
import com.bon.vo.Response;

/**
 * This class handles JSON-WPS requests in order to accomplish the shortest path
 * problem.
 * 
 * @author Mauricio Bonetti
 * 
 */
@Path("/easytrack")
public class Handler {
	private Request request = null;
	private SERVICE_INTERFACE serviceInterface = null;
	private EasyTrack service = EasyTrack.getInstance();

	/**
	 * This method is called when user reaches url: <server>/easytrack/ws. It
	 * handles a JSON-WSP request in order to accomplish the shortest path
	 * problem.
	 * 
	 * @param jsonRequest
	 * @return JSON-WPS response
	 */
	@POST
	@Path("/ws")
	@Produces(MediaType.APPLICATION_JSON)
	public String doService(String jsonRequest) {
		try {
			loadRequest(jsonRequest);
			if (serviceInterface.equals(SERVICE_INTERFACE.SAVE_MAP)) {
				return saveMap();
			} else {
				return findShortestPath();
			}
		} catch (Exception e) {
			return errorResponse(request, e);
		}
	}

	/**
	 * findShortestPath
	 * @return shortestPath JSON-WSP response.
	 */
	private String findShortestPath() {
		Map<String, Object> shortestPath = service.findShortestPath(request);
		Response response = ResponseUtil.loadSuccessFindShortestPathResponse(
				request, shortestPath);
		return JSONUtil.toJson(response);
	}

	/**
	 * saveMap
	 * @return saveMap JSON-WSP response.
	 */
	private String saveMap() {
		service.saveMap(request);
		Response response = ResponseUtil.loadSuccessSaveMapResponse(request);
		return JSONUtil.toJson(response);
	}

	/**
	 * This method loads the Request POJO from JSON request.
	 * @param jsonRequest
	 */
	private void loadRequest(String jsonRequest) {
		request = RequestUtil.loadRequest(jsonRequest);
		serviceInterface = RequestUtil.getServiceInterface(request
				.getMethodname());
		RequestValidator.assertRequest(jsonRequest, serviceInterface);
	}

	/**
	 * This method builds the error response in case of any Exception thrown.
	 * @param request
	 * @param e
	 * @return
	 */
	private String errorResponse(Request request, Exception e) {
		try {
			if (request == null) {
				return JSONUtil
						.toJson(ResponseUtil.loadDefaultErrorResponse(e));
			}
			Response response = ResponseUtil.loadErrorResponse(request, e);
			return JSONUtil.toJson(response);
		} catch (Exception ex) {
			return JSONUtil.toJson(ResponseUtil.loadDefaultErrorResponse(ex));
		}
	}
}
