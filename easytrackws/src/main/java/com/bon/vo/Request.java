package com.bon.vo;

import java.util.Map;

/**
 * This value object is the POJO representation of a JSON-WSP request
 * 
 * @author Mauricio Bonetti
 * 
 */
public class Request {
	private String type;
	private String version;
	private String methodname;
	private Map<String, Object> args;
	private Map<String, String> mirror;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getMethodname() {
		return methodname;
	}

	public void setMethodname(String methodname) {
		this.methodname = methodname;
	}

	public Map<String, Object> getArgs() {
		return args;
	}

	public void setArgs(Map<String, Object> args) {
		this.args = args;
	}

	public Map<String, String> getMirror() {
		return mirror;
	}

	public void setMirror(Map<String, String> mirror) {
		this.mirror = mirror;
	}

	// TODO: Refactor
	public String get(String key) {
		return getArgs().get(key) + "";
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(type);
		sb.append(";");
		sb.append(version);
		sb.append(";");
		sb.append(methodname);
		sb.append(";");
		sb.append(args);
		sb.append(";");
		sb.append(mirror);
		return sb.toString();
	}
}