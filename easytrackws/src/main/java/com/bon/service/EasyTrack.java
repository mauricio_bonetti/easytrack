package com.bon.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bon.util.JSONUtil;
import com.bon.vo.Request;
import com.bon.vo.Road;
import com.bon.vo.RoadMap;

/**
 * This is a service class that is responsible for calling easytrackcore in
 * order to accomplish the shortest path problem
 * 
 * @author Mauricio Bonetti
 * 
 */
public class EasyTrack {
	private static EasyTrack inst;

	private EasyTrack() {
	}

	/**
	 * Singleton!
	 * 
	 * @return
	 */
	public static EasyTrack getInstance() {
		if (inst == null) {
			inst = new EasyTrack();
		}
		return inst;
	}

	/**
	 * Calls easytrackcore's saveMap() method
	 * 
	 * @param request
	 */
	public void saveMap(Request request) {
		Map<String, Object> roadMap = request.getArgs();
		String jsonRoadmap = JSONUtil.toJson(roadMap);
		Core.saveMap(jsonRoadmap);
	}

	/**
	 * This method calls easytrackcore's findShortestPath() and findCost()
	 * 
	 * @param request
	 * @return Returns a map which keys are: <"path", List containing the
	 *         shortest path from request's source to destiny> and <"totalCost",
	 *         the trip's total cost>
	 */
	public Map<String, Object> findShortestPath(Request request) {
		RoadMap roadMap = Core.load(request.get("cityName"));
		String source = request.get("source");
		String destiny = request.get("destiny");
		List<Road> path = Core.findShortestPath(roadMap, source, destiny);

		List<String> responsePath = new ArrayList<String>();
		for (int i = 0; i < path.size(); i++) {
			String nextPoint = roadMap.getEdgeSource(path.get(i));
			responsePath.add(nextPoint);
			if (i == path.size() - 1) {
				String lastPoint = roadMap.getEdgeTarget(path.get(i));
				responsePath.add(lastPoint);
			}
		}

		double kmPerLitre = Double.parseDouble(request.get("autonomy"));
		double costPerLitre = Double.parseDouble(request.get("fuelCost"));
		double pathCost = Core.findCost(path, kmPerLitre, costPerLitre);

		Map<String, Object> result = new HashMap<String, Object>();
		result.put("path", responsePath);
		result.put("totalCost", pathCost);

		return result;
	}
}
