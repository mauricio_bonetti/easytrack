package com.bon.handler;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class HandlerTest {
	@Before
	public void setUp() {
		createTmpDir();
		System.setProperty("roadmap.file.dir", new File("tmp").getAbsolutePath());
	}
	
	private void createTmpDir() {
		File tmpDir = new File("tmp");
		if (!tmpDir.exists()) {
			try {
				FileUtils.forceMkdir(tmpDir);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	@Test
	public void doServiceTest_UnknownRequest() {
		String jsonRequest = "UnknownRequest";
		String actualResponse = new Handler().doService(jsonRequest);
		Assert.assertTrue(StringUtils.contains(actualResponse, "Unable to parse JSON to Java Object"));
	}
	
	@Test
	public void doServiceTest_SaveMapSuccess() {
		String jsonRequest = "{\"type\":\"jsonwsp/request\",\"version\":\"1.0\",\"methodname\":\"saveMap\",\"args\":{\"cityName\":\"Sorocaba\",\"roadMap\":[{\"source\":\"A\",\"destiny\":\"B\",\"distance\":639},{\"source\":\"B\",\"destiny\":\"C\",\"distance\":20}]},\"mirror\":{\"id\":1}}";
		String actualResponse = new Handler().doService(jsonRequest);
		Assert.assertTrue(StringUtils.contains(actualResponse, "\"result\":{\"success\":true"));
	}
	
	@Test
	public void doServiceTest_SaveMapError() {
		// wrong cityname key
		String jsonRequest = "{\"type\":\"jsonwsp/request\",\"version\":\"1.0\",\"methodname\":\"saveMap\",\"args\":{\"cityNames\":\"Sorocaba\",\"roadMap\":[{\"source\":\"A\",\"destiny\":\"B\",\"distance\":639},{\"source\":\"B\",\"destiny\":\"C\",\"distance\":20}]},\"mirror\":{\"id\":1}}";
		String actualResponse = new Handler().doService(jsonRequest);
		Assert.assertTrue(StringUtils.contains(actualResponse, "Invalid request from schema"));
	}
	
	@Test
	public void doServiceTest_findShortestPathSuccess() {
		String jsonSaveMapRequest = "{\"type\": \"jsonwsp/request\",\"version\": \"1.0\",\"methodname\": \"saveMap\",\"args\": {\"cityName\": \"Wall\",\"roadMap\": [{\"source\": \"A\",\"destiny\": \"B\",\"distance\": 10},{\"source\": \"B\",\"destiny\": \"D\",\"distance\": 15},{\"source\": \"A\",\"destiny\": \"C\",\"distance\": 20},{\"source\": \"C\",\"destiny\": \"D\",\"distance\": 30},{\"source\": \"B\",\"destiny\": \"E\",\"distance\": 50},{\"source\": \"D\",\"destiny\": \"E\",\"distance\": 30}]},\"mirror\": {\"id\": 2}\r\n}";
		// saving map "Wall"
		new Handler().doService(jsonSaveMapRequest);
		
		// map "Wall"
		// from A to D
		// autonomy 10
		// cost 2.50
		String jsonFindShortestPathRequest = "{\"type\": \"jsonwsp/request\",\"version\": \"1.0\",\"methodname\": \"findShortestPath\",\"args\": {\"cityName\": \"Wall\",\"source\": \"A\",\"destiny\": \"D\",\"autonomy\": 10,\"fuelCost\": 2.50},\"mirror\": {\"id\": 1}}";
		String actualResponse = new Handler().doService(jsonFindShortestPathRequest);
		
		String path = "path\":[\"A\",\"B\",\"D\"]";
		String totalCost = "totalCost\":6.25";
		
		Assert.assertTrue(StringUtils.contains(actualResponse, path));
		Assert.assertTrue(StringUtils.contains(actualResponse, totalCost));
	}
	
	@Test
	public void doServiceTest_findShortestPathError_FuelCostAsString() {
		String jsonSaveMapRequest = "{\"type\": \"jsonwsp/request\",\"version\": \"1.0\",\"methodname\": \"saveMap\",\"args\": {\"cityName\": \"Wall\",\"roadMap\": [{\"source\": \"A\",\"destiny\": \"B\",\"distance\": 10},{\"source\": \"B\",\"destiny\": \"D\",\"distance\": 15},{\"source\": \"A\",\"destiny\": \"C\",\"distance\": 20},{\"source\": \"C\",\"destiny\": \"D\",\"distance\": 30},{\"source\": \"B\",\"destiny\": \"E\",\"distance\": 50},{\"source\": \"D\",\"destiny\": \"E\",\"distance\": 30}]},\"mirror\": {\"id\": 2}\r\n}";
		// saving map "Wall"
		new Handler().doService(jsonSaveMapRequest);
		
		// wrong: fuel cost as String
		String jsonFindShortestPathRequest = "{\"type\": \"jsonwsp/request\",\"version\": \"1.0\",\"methodname\": \"findShortestPath\",\"args\": {\"cityName\": \"Wall\",\"source\": \"A\",\"destiny\": \"D\",\"autonomy\": 10,\"fuelCost\": \"adaba\"},\"mirror\": {\"id\": 1}}";
		String actualResponse = new Handler().doService(jsonFindShortestPathRequest);
		
		Assert.assertTrue(StringUtils.contains(actualResponse, "Invalid request from schema"));
	}
	
	@Test
	public void doServiceTest_findShortestPathError_NegativeFuelCost() {
		String jsonSaveMapRequest = "{\"type\": \"jsonwsp/request\",\"version\": \"1.0\",\"methodname\": \"saveMap\",\"args\": {\"cityName\": \"Wall\",\"roadMap\": [{\"source\": \"A\",\"destiny\": \"B\",\"distance\": 10},{\"source\": \"B\",\"destiny\": \"D\",\"distance\": 15},{\"source\": \"A\",\"destiny\": \"C\",\"distance\": 20},{\"source\": \"C\",\"destiny\": \"D\",\"distance\": 30},{\"source\": \"B\",\"destiny\": \"E\",\"distance\": 50},{\"source\": \"D\",\"destiny\": \"E\",\"distance\": 30}]},\"mirror\": {\"id\": 2}\r\n}";
		// saving map "Wall"
		new Handler().doService(jsonSaveMapRequest);
		
		// wrong: fuel cost as String
		String jsonFindShortestPathRequest = "{\"type\": \"jsonwsp/request\",\"version\": \"1.0\",\"methodname\": \"findShortestPath\",\"args\": {\"cityName\": \"Wall\",\"source\": \"A\",\"destiny\": \"D\",\"autonomy\": 10,\"fuelCost\": -10.43},\"mirror\": {\"id\": 1}}";
		String actualResponse = new Handler().doService(jsonFindShortestPathRequest);
		
		Assert.assertTrue(StringUtils.contains(actualResponse, "Invalid request from schema"));
	}
	
}
