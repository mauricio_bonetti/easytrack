package com.bon.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.bon.util.RequestValidator.SERVICE_INTERFACE;

@RunWith(JUnit4.class)
public class RequestValidatorTest {
	@Test
	public void assertRequestTest_saveMap() {
		String jsonRequest = "{\"type\":\"jsonwsp/request\",\"version\":\"1.0\",\"methodname\":\"saveMap\",\"args\":{\"cityName\":\"S\u00E3o Paulo\",\"roadMap\":[{\"source\":\"A\",\"destiny\":\"B\",\"distance\":10},{\"source\":\"B\",\"destiny\":\"C\",\"distance\":20}]},\"mirror\":{\"id\":1}}";
		RequestValidator.assertRequest(jsonRequest, SERVICE_INTERFACE.SAVE_MAP);
	}
	
	@Test(expected=RuntimeException.class)
	public void assertRequestTest_saveMap_Error() {
		// Distance as a String. Distance must be a number
		String invalidJsonRequest = "{\"type\":\"jsonwsp/request\",\"version\":\"1.0\",\"methodname\":\"saveMap\",\"args\":{\"cityName\":\"S\u00E3o Paulo\",\"roadMap\":[{\"source\":\"A\",\"destiny\":\"B\",\"distance\":\"10\"},{\"source\":\"B\",\"destiny\":\"C\",\"distance\":20}]},\"mirror\":{\"id\":1}}";
		RequestValidator.assertRequest(invalidJsonRequest, SERVICE_INTERFACE.SAVE_MAP);
	}
	
	@Test
	public void assertRequestTest_findShortestPath() {
		String jsonRequest = "{\"type\":\"jsonwsp/request\",\"version\":\"1.0\",\"methodname\":\"findShortestPath\",\"args\":{\"cityName\":\"S\u00E3o Paulo\",\"source\":\"A\",\"destiny\":\"B\",\"autonomy\":10,\"fuelCost\":10},\"mirror\":{\"id\":1}}";
		RequestValidator.assertRequest(jsonRequest, SERVICE_INTERFACE.FIND_SHORTEST_PATH);
	}
	
	@Test(expected=RuntimeException.class)
	public void assertRequestTest_findShortestPath_Error() {
		// Negative autonomy
		String ivalidJsonRequest = "{\"type\":\"jsonwsp/request\",\"version\":\"1.0\",\"methodname\":\"findShortestPath\",\"args\":{\"cityName\":\"S\u00E3o Paulo\",\"source\":\"A\",\"destiny\":\"B\",\"autonomy\":10,\"fuelCost\":-10},\"mirror\":{\"id\":1}}";
		RequestValidator.assertRequest(ivalidJsonRequest, SERVICE_INTERFACE.FIND_SHORTEST_PATH);
	}
}
